package fr.afpa.kawa.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.kawa.models.Cafes;

public class Preference {
    // Préférence = fichier XML
    // clé / valeur
    // /data/data/fr.afpa.kawa/preference.xml

    // TODO : 1) get instance Preference
    private static SharedPreferences getPreference(Context c) {
        return PreferenceManager.getDefaultSharedPreferences(c);
    }

    // TODO : 2) add Record (Café)
    public static boolean addCafe(Context c, Cafes.Records cafe) {
        List<Cafes.Records> recordsList = getCafes(c);

        if(!isInFavorite(recordsList, cafe.getFields().getNom_du_cafe())) {
            recordsList.add(cafe);

            save(c, recordsList);
            return true;
        } else {
            return false;
        }
    }

    private static void save(Context c, List<Cafes.Records> recordsList) {
        // convert to JSON
        Gson myGson = new Gson();
        String json = myGson.toJson(recordsList);

        // save DATA
        getPreference(c)
                .edit()
                .putString("favorites", json)
                .commit();
    }


    // TODO : 3) delete Record (Café)
    public static boolean removeCafe(Context c, List<Cafes.Records> favoriteList, Cafes.Records records) {
        String coffeeName = records.getFields().getNom_du_cafe();

        for (int i = 0; i < favoriteList.size(); i++) {
            if (favoriteList.get(i).getFields().getNom_du_cafe().equals(coffeeName)){
                favoriteList.remove(i);
                save(c, favoriteList);
                return true;
            }
        }
        return false;
    }

    // TODO : 4) liste des Records (Cafés)
    public static List<Cafes.Records> getCafes(Context c) {
        List<Cafes.Records> recordsList = new ArrayList<>();

        String json = getPreference(c).getString("favorites", null);

        if(json != null) {
            Gson myGson = new Gson();
            recordsList = myGson.fromJson(json, new TypeToken<List<Cafes.Records>>(){}.getType());
        }

        return recordsList;
    }

    // réinitialiser les preferences
    public static void clear(Context c) {
        getPreference(c)
                .edit()
                .clear()
                .commit();
    }

    public static boolean isInFavorite(List<Cafes.Records> favoriteList, String coffeeName){
        for (int i = 0; i < favoriteList.size(); i++) {
            if (favoriteList.get(i).getFields().getNom_du_cafe().equals(coffeeName)){
                return true;
            }
        }
        return false;
    }
}
