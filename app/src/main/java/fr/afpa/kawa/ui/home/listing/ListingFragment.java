package fr.afpa.kawa.ui.home.listing;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.mapbox.mapboxsdk.maps.MapView;

import fr.afpa.kawa.R;
import fr.afpa.kawa.models.Cafes;
import fr.afpa.kawa.ui.home.map.MapFragment;
import fr.afpa.kawa.utils.Preference;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListingFragment extends Fragment {
    private static final String PARAM_OBJECT_COFFEE = "PARAM_OBJECT_COFFEE";
    private Cafes myCoffees;
    private ListView listViewCafes;
    private CafeAdapter adapter;

    @Override
    public void onResume() {
        super.onResume();

        // actualisation
        adapter.notifyDataSetChanged();
    }

    public ListingFragment() {
        // Required empty public constructor
    }

    public static ListingFragment newInstance(Cafes myCoffees) {
        ListingFragment fragment = new ListingFragment();
        Bundle myBundle = new Bundle();
        myBundle.putSerializable(PARAM_OBJECT_COFFEE, myCoffees);
        fragment.setArguments(myBundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null) {
            myCoffees = (Cafes) getArguments().get(PARAM_OBJECT_COFFEE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View viewFragment = inflater.inflate(R.layout.fragment_listing, container, false);

        adapter = new CafeAdapter(
                getContext(),
                R.layout.item_listing,
                myCoffees.getRecords()
        );

        listViewCafes = viewFragment.findViewById(R.id.listViewCafes);
        listViewCafes.setAdapter(adapter);
        listViewCafes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Cafes.Records cafe = myCoffees.getRecords().get(position);
                if(Preference.addCafe(getContext(), cafe)) {
                    Toast.makeText(getContext(), "Café ajouté en favoris", Toast.LENGTH_SHORT).show();
                    adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(getContext(), "Ce Café est déjà en favoris", Toast.LENGTH_SHORT).show();
                }

                return false;
            }
        });

        return viewFragment;
    }

}
