package fr.afpa.kawa.ui.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import fr.afpa.kawa.R;
import fr.afpa.kawa.models.Cafes;
import fr.afpa.kawa.ui.favorite.FavoriteActivity;
import fr.afpa.kawa.ui.home.listing.ListingFragment;
import fr.afpa.kawa.ui.home.map.MapFragment;
import fr.afpa.kawa.ui.nonetwork.NoNetworkFragment;
import fr.afpa.kawa.utils.Constant;
import fr.afpa.kawa.utils.FastDialog;
import fr.afpa.kawa.utils.Network;

public class HomeActivity extends AppCompatActivity {
    private Button buttonMap;
    private Button buttonListing;

    private Cafes myCoffees;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        buttonMap = (Button) findViewById(R.id.buttonMap);
        buttonListing = (Button) findViewById(R.id.buttonListing);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(buttonListing.isActivated()) {
            // lancement recherche des cafés
            fetchCafes(buttonListing);
        } else {
            fetchCafes(buttonMap);
        }
    }

    private void fetchCafes(final View view) {

        if (Network.isNetworkAvailable(HomeActivity.this)) {
            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(this);
            String url = Constant.URL_CAFE;

            // Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String json) {

                            Gson myGson = new Gson();

                            myCoffees = myGson.fromJson(json, Cafes.class);

                            // clique sur le bouton de carte
                            view.performClick();
                            // ou showMap(buttonMap);

//                            Toast.makeText(
//                                    HomeActivity.this,
//                                    myCoffees.getRecords().get(0).getFields().getAdresse(),
//                                    Toast.LENGTH_SHORT
//                            ).show();
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    FastDialog.showDialog(
                            HomeActivity.this,
                            FastDialog.SIMPLE_DIALOG,
                            "Une erreur est survenue. Veuillez réessayer ultèrieurement."
                    );
                }
            });

// Add the request to the RequestQueue.
            queue.add(stringRequest);
        } else {
            //FastDialog.showDialog(HomeActivity.this, FastDialog.SIMPLE_DIALOG, "Vous devez être connecté");

            // affichage du fragment
            switchFragment(new NoNetworkFragment());
        }
    }

    public void showMap(View view) {
        if(!Network.isNetworkAvailable(HomeActivity.this)) {
            switchFragment(new NoNetworkFragment());
            return;
        }

        if(buttonMap.isActivated()) {
            return;
        }

        buttonMap.setActivated(true);
        buttonListing.setActivated(false);

        // changement de couleur de texte
        buttonMap.setTextColor(ContextCompat.getColor(HomeActivity.this, android.R.color.black));
        buttonListing.setTextColor(ContextCompat.getColor(HomeActivity.this, android.R.color.darker_gray));

        if(myCoffees != null) { // check cafés
            switchFragment(MapFragment.newInstance(myCoffees));
        } else {
            fetchCafes(buttonMap);
        }
    }

    public void showListing(View view) {
        if(!Network.isNetworkAvailable(HomeActivity.this)) {
            switchFragment(new NoNetworkFragment());
            return;
        }

        if(buttonListing.isActivated()) {
            return;
        }

        buttonListing.setActivated(true);
        buttonMap.setActivated(false);

        // changement de couleur de texte
        buttonListing.setTextColor(ContextCompat.getColor(HomeActivity.this, android.R.color.black));
        buttonMap.setTextColor(ContextCompat.getColor(HomeActivity.this, android.R.color.darker_gray));

        if(myCoffees != null) { // check cafés
            switchFragment(ListingFragment.newInstance(myCoffees));
        } else {
            fetchCafes(buttonListing);
        }
    }

    private void switchFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frameLayoutContainerFragment, fragment)
                .commit();
    }

    public void showFavorite(View view) {
        Intent intentFavorite = new Intent(HomeActivity.this, FavoriteActivity.class);
        startActivity(intentFavorite);
    }
}
