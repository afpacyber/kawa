package fr.afpa.kawa.ui.home.listing;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fr.afpa.kawa.R;
import fr.afpa.kawa.models.Cafes;
import fr.afpa.kawa.utils.Preference;

public class CafeAdapter extends ArrayAdapter<Cafes.Records> {

    private int resId;

    public CafeAdapter(Context context, int resource, List<Cafes.Records> objects) {
        super(context, resource, objects);

        resId = resource; // R.layout.item_listing
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // chargement du layout
        convertView = LayoutInflater.from(getContext()).inflate(resId, null);

        // récupération des Views
        TextView coffee_name = convertView.findViewById(R.id.coffee_name);
        TextView coffee_zipcode = convertView.findViewById(R.id.coffee_zipcode);
        ImageButton imageButtonStar = convertView.findViewById(R.id.imageButtonStar);

        // récupération de l'objet
        Cafes.Records item = getItem(position);

        // affichage des informations
        coffee_name.setText(item.getFields().getNom_du_cafe());
        coffee_zipcode.setText(item.getFields().getArrondissement());

        // TODO : ajouter une condition pour vérifier si le café est en favoris
        // TODO : modifier l'opacité avec la méthode setAlpha
        // si le café n'est pas en favoris

        List<Cafes.Records> cafeFavorites = Preference.getCafes(getContext());
        if(!Preference.isInFavorite(cafeFavorites,
                item.getFields().getNom_du_cafe())) {
            imageButtonStar.setAlpha(0.2f);
        }

        return convertView;
    }
}
