package fr.afpa.kawa.ui.favorite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import fr.afpa.kawa.R;
import fr.afpa.kawa.models.Cafes;
import fr.afpa.kawa.ui.home.listing.CafeAdapter;
import fr.afpa.kawa.utils.Preference;

public class FavoriteActivity extends AppCompatActivity {

    private ListView listViewCafes;
    private List<Cafes.Records> cafesList;
    private CafeAdapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);

        cafesList = Preference.getCafes(FavoriteActivity.this);

        adapter = new CafeAdapter(
                FavoriteActivity.this,
                R.layout.item_listing,
                cafesList
        );

        listViewCafes = findViewById(R.id.listViewCafes);

        listViewCafes.setAdapter(adapter);
        listViewCafes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Preference.removeCafe(FavoriteActivity.this, cafesList, cafesList.get(position));
                adapter.notifyDataSetChanged();
                return false;
            }
        });

    }
}
